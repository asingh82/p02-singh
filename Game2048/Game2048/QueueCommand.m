//
//  QueueCommand.m
//  Game2048
//
//  Created by Anshima on 07/02/17.
//  Copyright © 2017 AnshiCorp. All rights reserved.
//

#import "QueueCommand.h"

@implementation QueueCommand

+ (instancetype)commandWithDirection:(MoveDirection)direction
                     completionBlock:(void(^)(BOOL))completion {
    QueueCommand *command = [[self class] new];
    command.direction = direction;
    command.completion = completion;
    return command;
}

@end
