//
//  QueueCommand.h
//  Game2048
//
//  Created by Anshima on 07/02/17.
//  Copyright © 2017 AnshiCorp. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GameModel.h"

@interface QueueCommand : NSObject

@property (nonatomic) MoveDirection direction;
@property (nonatomic, copy) void(^completion)(BOOL atLeastOneMove);

+ (instancetype)commandWithDirection:(MoveDirection)direction completionBlock:(void(^)(BOOL))completion;

@end
