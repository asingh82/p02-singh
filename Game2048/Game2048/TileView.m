//
//  TileView.m
//  Game2048
//
//  Created by Anshima on 06/02/17.
//  Copyright © 2017 AnshiCorp. All rights reserved.
//

#import "TileView.h"
#import <UIKit/UIKit.h>

@implementation TileView

- (UIColor *)tileColorForValue:(NSUInteger)value {
    switch (value) {
        case 2:
            return [UIColor colorWithRed:238./255. green:128./255. blue:189./255. alpha:1];
        case 4:
            return [UIColor colorWithRed:200./255. green:114./255. blue:200./255. alpha:1];
        case 8:
            return [UIColor colorWithRed:242./255. green:100./255. blue:101./255. alpha:1];
        case 16:
            return [UIColor colorWithRed:245./255. green:128./255. blue:199./255. alpha:1];
        case 32:
            return [UIColor colorWithRed:246./255. green:124./255. blue:95./255. alpha:1];
        case 64:
            return [UIColor colorWithRed:246./255. green:94./255. blue:59./255. alpha:1];
        case 128:
        case 256:
        case 512:
        case 1024:
        case 2048:
            return [UIColor colorWithRed:237./255. green:207./255. blue:114./255. alpha:1];
        default:
            return [UIColor whiteColor];
    }
}

- (UIColor *)numberColorForValue:(NSUInteger)value {
    switch (value) {
        case 2:
        default:
            return [UIColor whiteColor];
    }
}

- (UIFont *)fontForNumbers {
    return [UIFont fontWithName:@"Arial" size:20];
}

@end
