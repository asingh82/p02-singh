//
//  ControlView.h
//  Game2048
//
//  Created by Anshima on 08/02/17.
//  Copyright © 2017 AnshiCorp. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ControlViewProtocol

- (void)upButtonTapped;
- (void)downButtonTapped;
- (void)leftButtonTapped;
- (void)rightButtonTapped;
- (void)resetButtonTapped;
- (void)exitButtonTapped;

@end

@interface ControlView : UIView

+ (instancetype)controlViewWithCornerRadius:(CGFloat)radius
                            backgroundColor:(UIColor *)color
                            movementButtons:(BOOL)moveButtonsEnabled
                                 exitButton:(BOOL)exitButtonEnabled
                                   delegate:(id<ControlViewProtocol>)delegate;

@end
