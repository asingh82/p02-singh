//
//  TileViewType.h
//  Game2048
//
//  Created by Anshima on 06/02/17.
//  Copyright © 2017 AnshiCorp. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TileViewProtocol;
@interface TileViewType : UIView

@property (nonatomic) NSInteger tileValue;

@property (nonatomic, weak) id<TileViewProtocol>delegate;

+ (instancetype)tileForPosition:(CGPoint)position
                     sideLength:(CGFloat)side
                          value:(NSUInteger)value
                   cornerRadius:(CGFloat)cornerRadius;

@end
