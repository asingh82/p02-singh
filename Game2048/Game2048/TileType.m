//
//  TileType.m
//  Game2048
//
//  Created by Anshima on 06/02/17.
//  Copyright © 2017 AnshiCorp. All rights reserved.
//

#import "TileType.h"

@implementation TileType

+ (instancetype)emptyTile {
    TileType *tile = [[self class] new];
    tile.empty = YES;
    tile.value = 0;
    return tile;
}

- (NSString *)description {
    if (self.empty) {
        return @"Tile (empty)";
    }
    return [NSString stringWithFormat:@"Tile (value: %lu)", (unsigned long)self.value];
}

@end
