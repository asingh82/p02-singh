//
//  TileView.h
//  Game2048
//
//  Created by Anshima on 06/02/17.
//  Copyright © 2017 AnshiCorp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
@protocol TileViewProtocol <NSObject>
-(UIColor *)tileColorForValue:(NSUInteger)value;
-(UIColor *)numberColorForValue:(NSUInteger)value;
-(UIFont *)fontForNumbers;
@end

@interface TileView : NSObject <TileViewProtocol>

@end
