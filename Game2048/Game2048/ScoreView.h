//
//  ScoreView.h
//  Game2048
//
//  Created by Anshima on 08/02/17.
//  Copyright © 2017 AnshiCorp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoreView : UIView

@property (nonatomic) NSInteger score;

+ (instancetype)scoreViewWithCornerRadius:(CGFloat)radius
                          backgroundColor:(UIColor *)color
                                textColor:(UIColor *)textColor
                                 textFont:(UIFont *)textFont;

@end
