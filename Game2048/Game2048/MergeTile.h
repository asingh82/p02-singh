//
//  MergeTile.h
//  Game2048
//
//  Created by Anshima on 07/02/17.
//  Copyright © 2017 AnshiCorp. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    MergeTileModeEmpty = 0,
    MergeTileModeNoAction,
    MergeTileModeMove,
    MergeTileModeSingleCombine,
    MergeTileModeDoubleCombine
} MergeTileMode;

@interface MergeTile : NSObject

@property (nonatomic) MergeTileMode mode;
@property (nonatomic) NSInteger originalIndexA;
@property (nonatomic) NSInteger originalIndexB;
@property (nonatomic) NSInteger value;

+ (instancetype)mergeTile;

@end
