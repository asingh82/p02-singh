//
//  ViewController.m
//  Game2048
//
//  Created by Anshima on 05/02/17.
//  Copyright © 2017 AnshiCorp. All rights reserved.
//

#import "ViewController.h"
#import "GameViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (IBAction)startGame:(id)sender{
    GameViewController *game = [GameViewController numberTileGameWithDimension:4
                                                    winThreshold:2048
                                                    backgroundColor:[UIColor whiteColor]
                                                    scoreModule:YES
                                                    buttonControls:NO
                                                    swipeControls:YES];
    [self presentViewController:game animated:YES completion:nil];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"/Users/anshima/Downloads/bg-flower.png"]]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
