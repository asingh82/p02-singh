//
//  TileType.h
//  Game2048
//
//  Created by Anshima on 06/02/17.
//  Copyright © 2017 AnshiCorp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TileType : NSObject

@property (nonatomic) BOOL empty;
@property (nonatomic) NSUInteger value;
+ (instancetype)emptyTile;

@end
