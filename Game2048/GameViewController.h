//
//  GameViewController.h
//  Game2048
//
//  Created by Anshima on 05/02/17.
//  Copyright © 2017 AnshiCorp. All rights reserved.
//

#import <UIKit/UIKit.h>




@protocol GameProtocol <NSObject>
- (void)gameFinishedWithVictory:(BOOL)didWin score:(NSInteger)score;
@end

@interface GameViewController : UIViewController

@property (nonatomic, weak) id<GameProtocol>delegate;

+ (instancetype)numberTileGameWithDimension:(NSUInteger)dimension
                               winThreshold:(NSUInteger)threshold
                            backgroundColor:(UIColor *)backgroundColor
                                scoreModule:(BOOL)scoreModuleEnabled
                             buttonControls:(BOOL)buttonControlsEnabled
                              swipeControls:(BOOL)swipeControlsEnabled;

@end

